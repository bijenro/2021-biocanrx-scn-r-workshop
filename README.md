# 2021 BioCanRx/SCN R Workshop

## Workshop Dates 
14:00-15:30 Eastern Time, September 22,29 October 6,13

## Workshop Venue
A Zoom meeting link has been sent to all invited meeting participants.

## Organizers		
* Dr. Megan Mahoney, BioCanRx, Director, Scientific Affairs and Training Programs
* Dr. Jonathan Draper,  Stem Cell Network, Program Director, Research & Partnerships,

## Workshop Files

* [Workshop Agenda](public/2021_BioCanRx-SCN_R_Workshop_Agenda_Posted.pdf?inline=false)

## Additional resources
* [EDX R Programming Courses](https://www.edx.org/learn/r-programming)
* [R Style guide by Hadley Wickham](https://style.tidyverse.org)
* [R language operators](https://www.statmethods.net/management/operators.html)
* [Assignment operators in R](https://www.r-bloggers.com/2010/11/assignment-operators-in-r)
* [An assortment of R cheat sheets](https://www.rstudio.com/resources/cheatsheets/)
* [CRAN R libraries](https://cran.r-project.org/)
* [Bioconductor bioinformatics libraries](https://www.bioconductor.org/)
* [SCN 2021 RNASeq Workshop Content](https://gitlab.com/ohri/2021-scn-rnaseq-workshop)

## Classes 

_Workshop files will be posted here on the day of each class_

### Class 1, Sept. 22: RStudio and the R Language

* [Class 1 presentation ](R_Class_1.pptx?inline=true)
* [Class 1 R walkthrough commands](public/Rwalkthrough.Rmd?inline=true)
* [Class 1 video](http://dropbox.ogic.ca/workshop_2021/Rclass1_2021.mp4)

### Class 2, Sept. 29: Data Frames, Libraries, Loading and Saving Data
* [Class 2 presentation ](R_Class_2.pptx?inline=true)
* [Class 2 R walkthrough commands](public/Rwalkthrough_class2.Rmd?inline=true)
* [Class 2 R homework and installs](public/post_class2_installs.R?inline=true)
* [Class 2 video](http://dropbox.ogic.ca/workshop_2021/Rclass2_2021.mp4)


### Class 3, Oct. 6: Accessing Cancer Experimental Data using TCGAbiolinks
* [Class 3 presentation ](public/R_Class_3.pptx?inline=true)
* [Class 3 R walkthrough commands](public/Rwalkthrough_class3.Rmd?inline=true)
* [Class 3 video](http://dropbox.ogic.ca/workshop_2021/Rclass3_2021.mp4)

### Class 4, Oct 13: Data analysis and Visualization
* [Class 4 presentation ](public/R_Class_4.pptx?inline=true)
* [Class 4 R walkthrough commands](public/Rwalkthrough_class4.Rmd?inline=true)
* _Class 4 video unavailable due to technical issues_

