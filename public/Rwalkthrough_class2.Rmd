
## Class 2 walkthrough

Sept 29, 2021

####  The first  quick review (and completion) of the functions session from last week

````{r}
# first we create a list of numeric values as we did last week
mynums<-c(1,2,3,4,5)  

# a simple function to convert Fareheit to celsius
ftoc<-function(far){
  cel<-(far-32)*5/9
  return(cel)
}

#a function is assigned to a variable.
#it takes the specified argument "far" which is evaluated in the function
#we can pass in a number or a variable containing an number
#it returns a value when completed.
#the curly bracketd delimint the function and can be nested
#if we type in just ftoc with no arguments, it returns the function code 

#calling the function with an input of 5 (degrees F)
#returns a value of 15.
ftoc(5)

#calling the function with an iput of -9999 (degrees F)
ftoc(-9999)

#can't meaningfully go below zero kelvin !
zerokelvin_f<- -459.67

#now we write a more complex function that handles invalid temperatures

#IF is a control structure
#
#if(the argument evaluates to true){
#  execute the code in the first curly bracket set
#}else{
#  execute the code in the second curly bracket set
#}
#
#this new function will return NA if less than zero kelvin
#As we're assigning it to the same variable ftoc it is overwriting the previous function!
ftoc<-function(far){
  if(far < zerokelvin_f){
    return(NA)
  }else{
    cel <- (far-32)*5/9
    return(cel)
  }
}

ftoc(-9999)

# what if we to round off the results
?round

#creating a new function that rounds by default
#but we can turn off rounding.
#this shows another feature of function arguments
#if defined with a value as in "rounddown=T" below the function 
#will use that as the default unless specified
#note that we can nest if statements; 
ftoc<-function(far,rounddown=T){
  if(far < zerokelvin_f){
    return(NA)
  }else{
    cel <- (far-32)*5/9
    if(rounddown==T){
      return(round(cel))
    }else{
      return(cel)
    }
  }
}

#looping over and printing the converted temperatures to the console
#using the for control structure
#"in" returns each element of the list in order over each ieration
#of the loop
for(num in mynums){
  print(ftoc(num))
}

#alernatively we can do it by indexing and counting from 1:5 
for(i in 1:length(mynums)){
    print(ftoc(mynums[i]))
}

#and there's a much easier way to do it
#by using sapply to operate on the list
#where we take the function we've written and 
# pass each value of the list to it.
?sapply
cvals<-sapply(mynums,ftoc) 
cvals

#can pass arguments into sapplied functions well!
cvals<-sapply(mynums,ftoc,rounddown=F) 
cvals
```


### The second section is a a review of dataframes and an introduction to plotting

```{r}
# Data frames are a set of vectors of equal length
library(datasets)

#there are a lot of built in datasets for teaching and testing
#a lot of these are "standard" statistical data sets
library(help = "datasets")

# we'll use a built in dataset of iris (flower data)
data(iris)
?iris

#lets look at the properties of the data

#head returns the first few rows (it can also be used for vectors, matrices)
head(iris)

#summary returns the properties of the columns
#five numeric columns and one Factor
summary(iris)

#row and column counts
nrow(iris)
ncol(iris)

#subset returns a portion of the data based on provided criteria
#it will return rows for which the criteria returns TRUE
#in this case the "==" operator tests if the value in the Species column is "setosa"
#https://www.statmethods.net/management/operators.html

iris_setosa<-subset(iris,Species=="setosa")
summary(iris_setosa)
nrow(iris_setosa)

#the subset can be more complicated. we use the logical operator & (and)
# to combine the tests 
# this returns a subset where Species is "setosa" AND Petal.Width>1
iris_setosa_pw1<-subset(iris,Species=="setosa" & Petal.Width>0.25)

#note that if we want to remove an object from our global environment we can use "rm"
rm(iris_setosa_pw1) 

#lets investigate the properties of the data.table
#this is a convenient way to look at numeric and categorical data
#for smaller tables. 
#this will plot every column versus every other column using the base graphics of R
plot(iris)

#useful for quick eyeballing of data
#what does this tell us about the properties of the data?
# there are species distinct distributions of values for each numeric column 
#some relationships, like Petal.Length vs Petal.Width are strongly correlated
#others like Sepal.Length vs Sepal.Width are more complex.

#We can plot a single relationship and properly label it
#again using base graphics functions
#?plot
plot(iris$Sepal.Length,iris$Sepal.Width,
     main="Sepal Length vs Width",
     xlab="Sepal Length",ylab="Sepal Width")

#we can make a prettier plot using ggplot2 functions
#ggplot has a visual "grammar" which allows you to build a plot 
#by "adding" (+) successive definitions
library(ggplot2)

#this is a plot of Sepal.Width vs Sepal.Length with points coloured
#bt the Species
#?ggplot
#?geom_point ?xlab ?ggtitle
ggplot(data=iris, aes(x = Sepal.Length, y = Sepal.Width)) + 
  geom_point(aes(color=Species)) + 
  xlab("Sepal Length") +  ylab("Sepal Width") +
  ggtitle("Sepal Length vs Width")

#we can update the plot to change the shape to species 
ggplot(data=iris, aes(x = Sepal.Length, y = Sepal.Width)) + 
  geom_point(aes(color=Species,shape=Species)) + 
  xlab("Sepal Length") +  ylab("Sepal Width") +
  ggtitle("Sepal Length vs Width")

#similarly we can use base graphics for a  histogram
#?hist
hist(iris$Sepal.Length, breaks=15,
     xlab="Sepal Length", ylab="Count", main="Sepal Length Histogram")

#or do a similarly fancy job with 
#?geom_histogram
ggplot(data=iris, aes(x=Sepal.Width)) + geom_histogram(binwidth=0.2, color="black", aes(fill=Species)) + 
  xlab("Sepal Length") +  ylab("Frequency") + ggtitle("Histogram of Sepal Length")

```
